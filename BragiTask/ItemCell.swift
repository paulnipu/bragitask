//
//  ItemCell.swift
//  BragiTask
//
//  Created by Arup Paul on 10/12/16.
//  Copyright © 2016 Arup Paul. All rights reserved.
//

import Foundation
import UIKit

class ItemCell:UITableViewCell{
    
    @IBOutlet private weak var lblTitle: UILabel!
    
    @IBOutlet private weak var imgCheckMark: UIImageView!
    
    
    func setItem(with:SampleDataModel){
        lblTitle.text = "\(with.item!)"
        if with.check == .Checked{
            imgCheckMark.image = UIImage(named: "yes")
        }else{
            imgCheckMark.image = UIImage(named: "no")
        }
    }
}
