//
//  SamleDataModel.swift
//  BragiTask
//
//  Created by Arup Paul on 10/12/16.
//  Copyright © 2016 Arup Paul. All rights reserved.
//

import Foundation

enum Section:Int{
    
    case Section1 = 0
    case Section2
    case Section3
    case Section4
}

enum Item:Int{
    
    case Item1 = 0
    case Item2
    case Item3
    case Item4
    case Item5
    case Item6
}

enum CheckMark:Int{
    case Unchecked = 0
    case Checked
}

class SampleDataModel {
    
    var section:Section?
    
    var item:Item?
    
    var check:CheckMark?
    
    var isValid:Bool{
        get {
            return check != nil && item != nil && section != nil
        }
    }
    
    init(val:Int,bitLength:Int = 8) {
        var str = String(val,radix:2)
        if str.characters.count < bitLength{
            let val = bitLength - str.characters.count
            str = String(repeating: "0", count: val) + str
        }
        check = CheckMark(rawValue: binaryToInt(binaryString: str.substring(to: str.index(str.startIndex, offsetBy: 1))))
        
        item = Item(rawValue: binaryToInt(binaryString: str[str.index(str.startIndex, offsetBy: 2)...str.index(str.startIndex, offsetBy: 5)]))
        
        //debugPrint(str[str.index(str.startIndex, offsetBy: 6)...str.index(str.startIndex, offsetBy: 7)])
        
        section = Section(rawValue: binaryToInt(binaryString: str[str.index(str.startIndex, offsetBy: 6)...str.index(str.startIndex, offsetBy: 7)]))
        
        
        //debugPrint("\(str) \(check!) \(item!) \(section!)")
    }
    
    private func binaryToInt(binaryString: String) -> Int {
        return Int(strtoul(binaryString, nil, 2))
    }
    
}
