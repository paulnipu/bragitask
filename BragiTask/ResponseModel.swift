//
//  ResponseModel.swift
//  BragiTask
//
//  Created by Arup Paul on 10/12/16.
//  Copyright © 2016 Arup Paul. All rights reserved.
//

import Foundation
/**
    Response data model for server response. Implement Serializable so can be serialized
 */
class ResponseModel: Serializable {
    var numbers = [Int]()
    
    required init(json: [String : AnyObject]) {
        if json["numbers"] != nil{
            numbers = json["numbers"] as! [Int]
        }else{
            debugPrint("Numbers format is wrong")
        }
        
    }

}
