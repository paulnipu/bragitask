//
//  NetworkUtil.swift
//  BragiTask
//
//  Created by Arup Paul on 10/12/16.
//  Copyright © 2016 Arup Paul. All rights reserved.
//

import Foundation

class NetworkUtil {
    typealias completionHandler<T> = (_ data:T?,_ error:String?) -> ()
     static let sharedInstance  = NetworkUtil()
    
    /**
     Load json data from network and serilize them into respective data model
     
     
     - parameters:
        - from: The URL, Can not be empty
        - handler: Handle the network response
     
     **Error is string.But for better handling can replace it with a Enum or Struct**
     */
    func getJsonData<T>(from:URL,handler:completionHandler<T>?) where T:Serializable{
        let task = URLSession.shared.dataTask(with: from, completionHandler: {(data, urlresponse, error) in
            guard data != nil else{
                DispatchQueue.main.async {
                    handler?(nil, "No response found.\nPlease pull to refresh")
                }

                return
            }
            do{
                if let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as? [String:AnyObject]{
                    let response = T(json: json)
                    DispatchQueue.main.async {
                        handler?(response,nil)
                    }
                    
                }
            }
            catch{
                DispatchQueue.main.async {
                    handler?(nil,"Error in parsing Data.\nPlease pull to refresh")
                }

            }
            
        })
        task.resume()
    }
    
    
}
