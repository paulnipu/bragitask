//
//  ViewController.swift
//  BragiTask
//
//  Created by Arup Paul on 10/12/16.
//  Copyright © 2016 Arup Paul. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    let cellIdentifier = "itemcell"
    
    private  var dataSource:DataSource?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupRefreshcontrol()
        self.dataSource = DataSource(data:nil)
        tableView.dataSource = self.dataSource
        
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startLoadingDataFromServer()
    }
    
    //Setting up the pull to refresh
    private func setupRefreshcontrol(){
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.backgroundColor = UIColor.lightGray
        tableView.refreshControl?.addTarget(self, action: #selector(onRefresh), for: .valueChanged)
    }
    
    //Call when user pull to refresh
    @objc private func onRefresh(){
        
        tableView.refreshControl?.beginRefreshing()
        startLoadingDataFromServer()
        
    }
    
    //Call Network utility for loading data
    private func startLoadingDataFromServer(){
        NetworkUtil.sharedInstance.getJsonData(from:URL(string:"http://foo.bragi.net/numbers.json")!, handler: {[unowned self](result:ResponseModel?,error) in
            self.tableView.refreshControl?.endRefreshing()
            guard error == nil else{
                debugPrint(error ?? "Error Occured")
                self.tableView.show(message: error ?? "Error Occured") //Extension method for UITableView to show a Message as backgroundview
                return
            }
            
            self.prepareData(result: result!)
        })
    }
    
    //Preapre data for data source. Create a Dictionory of data and sort the Item inside section and set the as the data in DataSource
    private func prepareData(result:ResponseModel){
        var dataMap = [Section:[SampleDataModel]]()
        
        result.numbers.forEach { (number) in
            let sampleItem = SampleDataModel(val: number)
            if sampleItem.isValid{
                if dataMap[(sampleItem.section)!] == nil{
                    dataMap[(sampleItem.section)!] = [SampleDataModel]()
                }
                dataMap[(sampleItem.section)!]?.append(sampleItem)
            }
            
            
            dataMap.keys.forEach { (section) in
                dataMap[section] = dataMap[section]?.sorted{
                    return ($0.item?.rawValue)! < ($1.item?.rawValue)!
                }
            }
        }
        //Push the data to  data source
        self.dataSource?.setNewData(dataMap)
        tableView.reloadData()
        
    }
    
}

