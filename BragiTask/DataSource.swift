//
//  DataSource.swift
//  BragiTask
//
//  Created by Arup Paul on 10/12/16.
//  Copyright © 2016 Arup Paul. All rights reserved.
//

import Foundation
import  UIKit
/**
    UITableView Datasource
 
 */
class DataSource:NSObject, UITableViewDataSource {
    
    let cellIdentifier = "itemcell"
    
    var dataMap:[(key:Section,value:[SampleDataModel])]?
    
    init(data:[Section:[SampleDataModel]]?){
        //dataMap = data.flatMap({$0})
        super.init()
        if let data = data{
            self.setNewData(data)
        }
    }
    
    func setNewData(_ with:[Section:[SampleDataModel]]){
        dataMap = with.sorted { (item1, item2) -> Bool in
            return item1.key.rawValue < item2.key.rawValue
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let val = dataMap?.count ?? 0
        if val == 0{
            tableView.show(message: "No data for display.\nPull to refresh")
        }else{
            tableView.hideMessage()
        }
        return val
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let val = dataMap?[section].value.count ?? 0
        return val
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let key = dataMap?[section].key
        return "\(key!)"
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier,for:indexPath) as? ItemCell
        if let item = dataMap?[indexPath.section].value[indexPath.row]{
            cell?.setItem(with: item)
        }
        return cell!
    }
}
