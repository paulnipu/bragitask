//
//  UITableViewExtension.swift
//  BragiTask
//
//  Created by Arup Paul on 10/12/16.
//  Copyright © 2016 Arup Paul. All rights reserved.
//

import Foundation
import UIKit

extension UITableView{
    
    /**
     Show a message in TableView
     - parameters:
        - message: Message to show. Can not be nil
     */
    func show(message:String){
        //self.backgroundView = nil
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height))
        label.text = message
        label.numberOfLines = 0
        label.textColor = UIColor.gray
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textAlignment = .center
        self.separatorStyle = .none
        self.backgroundView = label
    }
    
    func hideMessage(){
        self.backgroundView = nil
        self.separatorStyle = .singleLine

    }
}
