//
//  Serializable.swift
//  BragiTask
//
//  Created by Arup Paul on 10/12/16.
//  Copyright © 2016 Arup Paul. All rights reserved.
//

import Foundation
protocol Serializable {
    
    init(json:[String:AnyObject])
}
